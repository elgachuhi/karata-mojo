var karata = {};
var Game = function(){
  var deck = [
    {
      "uid":"AH",
      "img": "ace_of_hearts",
      "value":""
    },
    {
      "uid":"AC",
      "img": "ace_of_clubs",
      "value":""
    },
    {
      "uid":"AD",
      "img": "ace_of_diamonds",
      "value":""
    },
    {
      "uid":"AS",
      "img": "ace_of_spades",
      "value":""
    },
    {
      "uid":"2H",
      "img": "2_of_hearts",
      "value":""
    },
    {
      "uid":"2C",
      "img": "2_of_clubs",
      "value":""
    },
    {
      "uid":"2D",
      "img": "2_of_diamonds",
      "value":""
    },
    {
      "uid":"2S",
      "img": "2_of_spades",
      "value":""
    },
    {
      "uid":"3H",
      "img": "3_of_hearts",
      "value":""
    },
    {
      "uid":"3C",
      "img": "3_of_clubs",
      "value":""
    },
    {
      "uid":"3D",
      "img": "3_of_diamonds",
      "value":""
    },
    {
      "uid":"3S",
      "img": "3_of_spades",
      "value":""
    },
    {
      "uid":"4H",
      "img": "4_of_hearts",
      "value":""
    },
    {
      "uid":"4C",
      "img": "4_of_clubs",
      "value":""
    },
    {
      "uid":"4D",
      "img": "4_of_diamonds",
      "value":""
    },
    {
      "uid":"4S",
      "img": "4_of_spades",
      "value":""
    },
    {
      "uid":"5H",
      "img": "5_of_hearts",
      "value":""
    },
    {
      "uid":"5C",
      "img": "5_of_clubs",
      "value":""
    },
    {
      "uid":"5D",
      "img": "5_of_diamonds",
      "value":""
    },
    {
      "uid":"5S",
      "img": "5_of_spades",
      "value":""
    },
    {
      "uid":"6H",
      "img": "6_of_hearts",
      "value":""
    },
    {
      "uid":"6C",
      "img": "6_of_clubs",
      "value":""
    },
    {
      "uid":"6D",
      "img": "6_of_diamonds",
      "value":""
    },
    {
      "uid":"6S",
      "img": "6_of_spades",
      "value":""
    },
    {
      "uid":"7H",
      "img": "7_of_hearts",
      "value":""
    },
    {
      "uid":"7C",
      "img": "7_of_clubs",
      "value":""
    },
    {
      "uid":"7D",
      "img": "7_of_diamonds",
      "value":""
    },
    {
      "uid":"7S",
      "img": "7_of_spades",
      "value":""
    },
    {
      "uid":"8H",
      "img": "8_of_hearts",
      "value":""
    },
    {
      "uid":"8C",
      "img": "8_of_clubs",
      "value":""
    },
    {
      "uid":"8D",
      "img": "8_of_diamonds",
      "value":""
    },
    {
      "uid":"8S",
      "img": "8_of_spades",
      "value":""
    },
    {
      "uid":"9H",
      "img": "9_of_hearts",
      "value":""
    },
    {
      "uid":"9C",
      "img": "9_of_clubs",
      "value":""
    },
    {
      "uid":"9D",
      "img": "9_of_diamonds",
      "value":""
    },
    {
      "uid":"9S",
      "img": "9_of_spades",
      "value":""
    },
    {
      "uid":"10H",
      "img": "10_of_hearts",
      "value":""
    },
    {
      "uid":"10C",
      "img": "10_of_clubs",
      "value":""
    },
    {
      "uid":"10D",
      "img": "10_of_diamonds",
      "value":""
    },
    {
      "uid":"10S",
      "img": "10_of_spades",
      "value":""
    },
    {
      "uid":"JH",
      "img": "jack_of_hearts",
      "value":""
    },
    {
      "uid":"JC",
      "img": "jack_of_clubs",
      "value":""
    },
    {
      "uid":"JD",
      "img": "jack_of_diamonds",
      "value":""
    },
    {
      "uid":"JS",
      "img": "jack_of_spades",
      "value":""
    },
    {
      "uid":"QH",
      "img": "queen_of_hearts",
      "value":""
    },
    {
      "uid":"QC",
      "img": "queen_of_clubs",
      "value":""
    },
    {
      "uid":"QD",
      "img": "queen_of_diamonds",
      "value":""
    },
    {
      "uid":"QS",
      "img": "queen_of_spades",
      "value":""
    },
    {
      "uid":"KH",
      "img": "king_of_hearts",
      "value":""
    },
    {
      "uid":"KC",
      "img": "king_of_clubs",
      "value":""
    },
    {
      "uid":"KD",
      "img": "king_of_diamonds",
      "value":""
    },
    {
      "uid":"KS",
      "img": "king_of_spades",
      "value":""
    },
  ]

  this.shuffle = function(){
    var tmp = [];
    while(tmp.length !== deck.length){
      var randIndex = Math.floor((Math.random() * deck.length - 1) + 1);

      if (tmp.indexOf(deck[randIndex]) === -1){
        tmp.push(deck[randIndex]);
      }
    }

    deck = tmp;
  }
  var handCount = 0;
  this.giveHand = function(){
    if(handCount >= 2) return;

    var rand1 = Math.floor((Math.random() * deck.length - 1) + 1),
        rand2 = rand1 === 0 ? 5 : 4;
    slice = deck.splice(rand1, 4);

    handCount += 1;
    return slice;
  }

  this.getFirstRandom = function(){
    var rand1 = Math.floor((Math.random() * deck.length - 1) + 1);
    // TODO change this to a regex
    var cantStart = [
      "AS","AH","AD","AC","2S","2H","2D","2C",
      "3S","3H","3D","3C","8S","8H","8D","8C",
      "JS","JH","JD","JC","QS","QH","QD","QC",
      "KS","KH","KD","KC"
    ];

    if(cantStart.indexOf(deck[rand1].uid) === -1 ){
      return deck[rand1];
    }
    console.log('continue recursing')
    return this.getFirstRandom();
  };

  this.getStack = function(){
    return deck;
  };

  this.draw = function(){
    if (deck.length === 0) return 0;
    return deck.splice(deck.length -1, 1);
  }
}

karata.Game = Game;

