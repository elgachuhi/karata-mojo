var game = new karata.Game();
var player1 = new karata.Player('1');
var player2 = new karata.Player('2');

function start(){
  game.shuffle();
  console.log("Initial Deck: ", game.getStack().length);
  player1.hand = game.giveHand();
  player2.hand = game.giveHand();
  return game.getFirstRandom();
}

/*
 * Driver
 * interaction between instances of game and player
 * and deck
 */

document.getElementById('deck-top-img')
  .addEventListener('dragstart', function(event){
    event.dataTransfer.effectAllowed = 'move';
});

document.getElementById('start').addEventListener('click',function(){
  var intialCard = start();
    console.log("Deck after start: ", game.getStack().length);
    var img = document.createElement('img');
    img.src =  "images/"+intialCard.img+'.png';

    document.getElementById('current-card').appendChild(img);
    document.getElementById("start").style.display = "none";

  for(var i = 0; i <= player1.hand.length -1; i++) (function(n){
    var img = document.createElement('img');

    img.src = 'images/'+player1.hand[n].img + ".png";
    img.id  = player1.hand[n].uid;
    img.setAttribute('draggable', true);
    img.setAttribute('data-playerid', player1.pid);

    img.addEventListener('dragstart', function(event){
      event.dataTransfer.effectAllowed = 'move';
      event.dataTransfer.setData('text', player1.hand[n].uid);
    });

    document.getElementById('hand').appendChild(img);
  })(i)

  for(var i = 0; i <= player2.hand.length -1; i++) (function(n){
    var img = document.createElement('img');
    img.src = 'images/'+player2.hand[n].img + ".png";
    img.id = player2.hand[n].uid;

    img.setAttribute('draggable', true);
    img.setAttribute('data-playerid', player2.pid);

    img.addEventListener('dragstart', function(event){
      event.dataTransfer.effectAllowed = 'move';
      event.dataTransfer.setData('text', player2.hand[n].uid);
    });

    document.getElementById('hand2').appendChild(img);
  })(i)

});

var dropZones = [
  document.getElementById('current-card'),
  document.getElementById('hand')
],
l = dropZones.length;
for(var i = 0; i <= l - 1; i++){
  dropZones[i].addEventListener('dragover', function(event){
    if(event.preventDefault){ event.preventDefault(); }
    event.dataTransfer.dropEffect = 'move';
    return false;
  });

  dropZones[i].addEventListener('dragleave', function(event){
    this.style.border = "";
  });

  dropZones[i].addEventListener('dragenter', function(event){
    if(event.stopPropagation){ event.stopPropagation(); }
    this.style.border ="1px solid green";
  });

   dropZones[i].addEventListener('drop', function(evt){
    if(evt.preventDefault) evt.preventDefault();
    if(evt.stopPropagation) evt.stopPropagation();

    evt.target.style.border = "";

    var dragged = document.getElementById(evt.dataTransfer.getData('text'));
    if(this.id === "current-card") {
      this.innerHTML = "";
      dragged.dataset.playerid === "1" ? player1.drop(dragged.id) : player2.drop(dragged.id);
    }

    if(this.id === 'hand'){
      var card = game.draw()[0],
          img  = document.createElement('img');

      img.src = "images/" + card.img + ".png";
      img.id  = card.uid;

      img.setAttribute('draggable', true);
      img.setAttribute('data-playerid', player1.pid);

      player1.draw(card);
      dragged = img;
    }

    console.log('Deck after Drop: ', game.getStack().length);

    this.appendChild(dragged.cloneNode(true));
    dragged.remove();
  });

}


