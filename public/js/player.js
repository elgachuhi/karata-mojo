function Player(pid){
  this.hand = [];
  this.kadi = false;

  this.drop = function(cardid){
    this.hand.sort(function(card){ return card.uid === cardid})
    var index = this.hand.indexOf(this.hand[this.hand.length - 1]);

    if(index !== -1){
      this.hand.splice(index, 1);
      console.log(this.hand);
      return;
    }

    throw "Card not found on player " + pid + " hand";
  };

  this.draw = function(card){
    this.hand.push(card);
  };

  this.declareCard = function(){
    this.kadi = true;
  };

  this.pid = pid;
}

karata.Player = Player;
